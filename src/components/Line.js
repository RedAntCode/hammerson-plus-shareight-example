import React from 'react'

function Line (props) {
  return <div style={{ margin: 10 }} {...props} />
}

export default Line
