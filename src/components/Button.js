import React from 'react'

function Button (props) {
  return (
    <button
      style={{
        padding: 5,
        backgroundColor: '#262a30',
        color: 'white',
        margin: '0 5px',
        width: 50
      }}
      {...props}
    />
  )
}
export default Button
