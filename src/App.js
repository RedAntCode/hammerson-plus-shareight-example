import React from 'react'
import Button from './components/Button'
import Line from './components/Line'

function App () {
  const [count, setCount] = React.useState(0)
  const [data, setData] = React.useState({})
  const subtractOne = React.useCallback(() => setCount((x) => x - 1), [])
  const addOne = React.useCallback(() => setCount((x) => x + 1), [])
  React.useEffect(() => {
    fetch(`${process.env.PUBLIC_URL}/esi`)
      .then(res => res.json())
      .then(data => setData({ header: data.header, footer: data.footer }))
      .catch(err => console.log({ err }))
  }, [])

  if (typeof window !== 'undefined') {
    console.log('WINDOW __HOST__', window.__host__)
  }

  if (Object.keys(data).length === 0) return <div>Fetching esi...</div>
  return (
    <>
      {data.header && <div dangerouslySetInnerHTML={{ __html: data.header }} /> }
      <div
        style={{
          padding: 40,
          textAlign: "center"
        }}
      >
        <Line>Example react app:</Line>
        <Line>
          <Button onClick={subtractOne}>-1</Button>
          <Button onClick={addOne}>+1</Button>
        </Line>
        <Line style={{ fontSize: "2em", fontWeight: "bold" }}>{count}</Line>
      </div>
      {data.footer && <div dangerouslySetInnerHTML={{ __html: data.footer }} /> }
    </>
  )
}

export default App
