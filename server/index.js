if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}
const express = require('express')
const app = express()
const fs = require('fs')
const path = require('path')
const cors = require('cors')
require('isomorphic-unfetch')
const PORT = process.env.PORT || 4000

app.use(express.static('./build'))
app.use(cors())

app.get('/esi', async (req, res) => {
  try {
    const header = await fetch(`${process.env.HAM_URL}/esi-header`)
    const footer = await fetch(`${process.env.HAM_URL}/esi-footer`)
    const headerText = await header.text()
    const footerText = await footer.text()
    res.send({
      header: headerText,
      footer: footerText
    })
  } catch (e) {
    console.error('esi fetch error', e)
  }
})

app.get('/test', async (req, res) => {
  try {
    const header = await fetch(`${process.env.HAM_URL}/esi-header`)
    const footer = await fetch(`${process.env.HAM_URL}/esi-footer`)
    const headerText = await header.text()
    const footerText = await footer.text()
    const reactHtml = fs.readFileSync('./build/index.html', 'utf8')
    const testHtml = '<html><body><div>Demo app</div></body></html>'

    const reactBody = reactHtml.slice(reactHtml.indexOf('<body>') + 6, reactHtml.indexOf('</body>'))
    const footerBody = footerText.slice(footerText.indexOf('<body>') + 6, footerText.indexOf('</body>'))
    const headerBody = headerText.slice(headerText.indexOf('<body>') + 6, headerText.indexOf('</body>'))

    const totalHtml = headerBody.replace('</body>', reactBody + footerBody + '</body>')

    res.send(totalHtml)
  } catch (err) {
    console.error('server error', err)
    res.status(500).send('error')
  }
})

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../build', 'index.html'))
})

app.listen(PORT, () => {
  console.log('server is listening on port: ', PORT)
})
